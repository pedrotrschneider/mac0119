'''
/******************************************************************************/
/*                                 Classe 03                                  */
/*                                                                            */
/* @brief Código feito para tratar dados e plotar gráficos relevantes para    */
/*        a entrega do exercício de MAE0119 Classe 03.                        */
/*                                                                            */
/* @authors Pedro Tonini Rosenberg Schneider,                                 */
/*          Guilherme Vinicius Ferreira de Assis.                             */
/*                                                                            */
/* @dependencies Esse programa depende apenas do módulo matplotlib de python. */
/*               Para mais informações sobre como instalá-lo, veja:           */
/*               <https://matplotlib.org/stable/users/installing.html>        */
/*                                                                            */
/* @usage                                                                     */
/* $ python3 main.py                                                          */
/*                                                                            */
/* @license GNU GPLv3                                                         */
/*                                                                            */
/*    This program is free software: you can redistribute it and/or modify    */
/*    it under the terms of the GNU General Public License as published by    */
/*    the Free Software Foundation, either version 3 of the License, or       */
/*    (at your option) any later version.                                     */
/*                                                                            */
/*    This program is distributed in the hope that it will be useful,         */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*    GNU General Public License for more details.                            */
/*                                                                            */
/*    You should have received a copy of the GNU General Public License       */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.  */
/*                                                                            */
/******************************************************************************/
'''

import csv
import matplotlib.pyplot as plot


def get_col(data:list, col:int, integer=False, clean=True) -> list:
    '''
    /********************************************************************************
     * 
     * @brief Retira uma coluna determinada por (row) da matriz (data), pulando 
     *        a primeira entrada da coluna para levar em conta o cabeçalho dos dados.
     *
     * @param data Matriz de dados a ser retirada uma coluna.
     * @param col Determina qual a coluna a ser retirada da matriz. Indexado
     *            em 0, da esquerda para a direita.
     * @param integer Determina se os dados devem ser convertidos
     *                para inteiros (True) ou não (False);
     * @param clean Determia se os dados faltantes (-1) devem ser retirados ou mantidos.
     * 
     * @return Retorna uma lista com com a coluna de dados.
     *
     *******************************************************************************/
    '''
    out = []
    inp = 0
    for item in data:
        if inp != 0:
            if not integer:
                out.append(item[col])
            else:
                if (not clean) or int(item[col]) >= 0:
                    out.append(int(item[col]))
        inp += 1
    return out


def calc_freq(data: list) -> (list, list):
    '''
    /******************************************************************************
     * 
     * @brief Calcula a frequência absoluta dos dados de uma lista com valores
     *        discretos.
     * 
     * @param data Matriz de dados a ser retirada uma coluna.
     * 
     * @return Retorna um touple de duas listas, a primeira sendo as diferentes
     *         categorias que foram econtradas e a segunda a frequência absoluta
     *         de cada uma das categorias. As listas são alinhadas.
     *
     *****************************************************************************/
    '''
    dict = {}
    for item in data:
        if not item in dict:
            dict[item] = 0
        dict[item] = dict[item] + 1

    keys = list(dict)
    vals = []
    for key in dict:
        vals.append(dict[key])
    return (keys, vals)


def calc_freq_ranges(data: list, ranges: list) -> (list, list):
    '''
    /******************************************************************************************
     * 
     * Calcula a frequência absoluta dos dados de uma lista com valores contínuos, dada
     * uma lista de intervalos de valores
     * 
     * @param data Matriz de dados a ser retirada uma coluna.
     * @param ranges Lista de intervalos de valores a serem considerados no cálculo da
     *               frequência abosluta.
     * 
     * @return Retorna um touple de duas listas, a primeira sendo as diferentes
     *         categorias que foram econtradas e a segunda a frequência absoluta
     *         de cada uma das categorias. As listas são alinhadas.
     *
     *****************************************************************************************/
    '''

    labels = []
    vals = []
    for l in range(len(ranges) - 1):
        labels.append(f"{ranges[l]} a {ranges[l+1]}")
        vals.append(0)
    
    for item in data:
        for l in range(len(ranges) - 1):
            if ranges[l] <= item < ranges[l+1]: vals[l] += 1
    
    return (labels, vals)


def main():
    # Lendo arquivo .csv
    data = []
    with open("../data/respostas.csv", newline="") as file:
        data = list(csv.reader(file))
    
    ''' *********************************************
    Calculado e plotando as informações sobre irmãos.
    ********************************************* '''
    # Tratando os dados:
    sibling_col = get_col(data, 4)
    sibling_lables, sibling_data = calc_freq(sibling_col)
    for i in range(len(sibling_lables)):
        suffix = " irmão"
        if(sibling_lables[i] != "1"): suffix += "s"
        sibling_lables[i] += suffix
    print("Irmãos: ", sibling_lables, sep="")
    print("        ", sibling_data, sep="")

    # Plotando os gráficos:
    plot.pie(sibling_data, # Dados para plotar o gráfico.
             labels=sibling_lables, # Labels para os dados. Deve estar alinhada com a lista de dados.
             shadow=True, # Desenha uma sombra em baixo do gráfico.
             autopct="%1.1f%%" # Faz com que as porcentagens sejam desenhadas, com precisão de uma casa decimal.
    )
    plot.suptitle("Distribuição de frequência de irmãos") # Título que aparece na parte superior do gráfico
    plot.show() # Mostra uma janela com o gráfico. Tem que ter uma chamada pra essa função toda vez que plotar um gráfico.

    ''' *********************************************
    Calculado e plotando as informações sobre peso.
    ********************************************* '''
    # Tratando os dados:
    weight_col = get_col(data, 3, True)
    weight_labels, weight_data = calc_freq_ranges(weight_col, range(40, 132, 10))
    print("Peso: ", weight_labels, sep="")
    print("      ", weight_data, sep="")

    # Plotando o gráfico:
    plot.hist(weight_col, # Dados para plotar o histograma.
              range(40, 160, 10), # Intervalos de valores para o gráfico.
              density=True, # Mostra os dados na forma de densidade de frequência.
              edgecolor='black', # Mostra um outline preto nas caixas.
              linewidth=1.2 # Determina a espessura do outline nas caixas.
    )
    plot.xlim([38, 132])  # Determina os limites superior e inferior do eixo x.
    plot.xticks(range(40, 132, 10)) # Determina quais serão os valores marcados no eixo x.
    plot.suptitle("Histograma de pesos") # Título que aparece na parte superior do gráfico.
    plot.xlabel("Peso (Kg)")  # Label do eixo x.
    plot.ylabel("Densidade de frequência") # Label do eixo y.
    plot.show() # Mostra uma janela com o gráfico. Tem que ter uma chamada pra essa função toda vez que plotar um gráfico.

    ''' **************************************************************
    Calculado e plotando as informações sobre peso dividido por sexo.
    ************************************************************** '''
    # Tratando os dados:
    weight_col = get_col(data, 3, True, False)
    sex_col = get_col(data, 1)
    fem_weight = []
    masc_weight = []
    for i in range(len(sex_col)): # Separando os pesos de cada sexo em listas diferentes.
        item = sex_col[i]
        if weight_col[i] < 0: continue
        if item == "masculino":
            masc_weight.append(weight_col[i])
        else:
            fem_weight.append(weight_col[i])
    
    # Plotando os gráficos:
    plot.hist(fem_weight, # Dados para plotar o histograma.
              range(40, 160, 10), # Intervalos de valores para o gráfico.
              density=True, # Mostra os dados na forma de densidade de frequência.
              edgecolor='black', # Mostra um outline preto nas caixas.
              linewidth=1.2 # Determina a espessura do outline nas caixas.
    )
    plot.xlim([38, 132])  # Determina os limites superior e inferior do eixo x.
    plot.xticks(range(40, 132, 10)) # Determina quais serão os valores marcados no eixo x.
    plot.suptitle("Histograma de pesos femininos") # Título que aparece na parte superior do gráfico.
    plot.xlabel("Peso (Kg)")  # Label do eixo x.
    plot.ylabel("Densidade de frequência") # Label do eixo y.
    plot.show() # Mostra uma janela com o gráfico. Tem que ter uma chamada pra essa função toda vez que plotar um gráfico.

    plot.hist(masc_weight, # Dados para plotar o histograma.
              range(40, 160, 10), # Intervalos de valores para o gráfico.
              density=True, # Mostra os dados na forma de densidade de frequência.
              edgecolor='black', # Mostra um outline preto nas caixas.
              linewidth=1.2 # Determina a espessura do outline nas caixas.
    )
    plot.xlim([38, 132]) # Determina os limites superior e inferior do eixo x.
    plot.xticks(range(40, 132, 10)) # Determina quais serão os valores marcados no eixo x.
    plot.suptitle("Histograma de pesos masculinos") # Título que aparece na parte superior do gráfico.
    plot.xlabel("Peso (Kg)") # Label do eixo x.
    plot.ylabel("Densidade de frequência") # Label do eixo y.
    plot.show() # Mostra uma janela com o gráfico. Tem que ter uma chamada pra essa função toda vez que plotar um gráfico.

    plot.boxplot([fem_weight, masc_weight], # Lista de dados. Cada lista será representada como um boxplot diferente.
                 manage_ticks=True, # Atualiza os ticks no eixo de dados dependendo dos valores do boxplot.
                 showfliers=True, # Mostra os outliers no gráfico.
                 sym="x", # Define o símbolo que será usado para mostrar os outliers.
                 patch_artist=True, # Pinta a caixa do boxplot (é possível configurar a cor).
                 labels=["feminino", "masculino"] # Lista de labels para o gráfico. Cada string representa a label de um boxplot diferente.
    )
    plot.suptitle("Boxplot dos pesos por sexo") # Título que aparece na parte superior do gráfico.
    plot.yticks(range(40, 132, 10)) # Determina quais serão os valores marcados no eixo y.
    plot.grid() # Mostra a grid no gráfico.
    plot.xlabel("Sexo") # Label do eixo x.
    plot.ylabel("Peso (Kg)") # Label do eixo y.
    plot.show() # Mostra uma janela com o gráfico. Tem que ter uma chamada pra essa função toda vez que plotar um gráfico.

    ''' ****************************************************
    Calculado e plotando as informações sobre peso e altura.
    **************************************************** '''
    # Tratando os dados:
    weight_col = get_col(data, 3, True, False)
    height_col = get_col(data, 2, True, False)
    for i in range(len(weight_col)):
        item = weight_col[i]
        if item == -1:
            weight_col.pop(i)
            height_col.pop(i)
            break

    # Plotando o gráfico:
    plot.scatter(weight_col, # Dados para o eixo x do diagrama de disperssão.
                 height_col # Dados para o eixo y do diagrama de disperssão.
    )
    plot.suptitle("Diagrama de disperssão de Peso por Altura") # Título que aparece na parte superior do gráfico.
    plot.grid() # Mostra a grid no gráfico.
    plot.xlabel("Peso (Kg)") # Label do eixo x.
    plot.ylabel("Altura (cm)") # Label do eixo y.
    plot.show() # Mostra uma janela com o gráfico. Tem que ter uma chamada pra essa função toda vez que plotar um gráfico.

    ''' ************************************************************
    Calculando informações sobre ganho de peso na pandemia por sexo.
    ************************************************************ '''
    # Tratando os dados
    pandemic_col = get_col(data, 8, True, False)
    sex_col = get_col(data, 1)
    fem_pandemic = []
    masc_pandemic = []
    for i in range(len(sex_col)): # Separando os pesos de cada sexo em listas diferentes.
        item = sex_col[i]
        if pandemic_col[i] < 0: continue
        if item == "masculino":
            masc_pandemic.append(pandemic_col[i])
        else:
            fem_pandemic.append(pandemic_col[i])
    pandemic_labels, pandemic_data = calc_freq(get_col(data, 8, True, True))
    fem_pandemic_labels, fem_pandemic_data = calc_freq(fem_pandemic)
    masc_pandemic_labels, masc_pandemic_data = calc_freq(masc_pandemic)

    print("Feminino:")
    print(fem_pandemic_labels)
    print(fem_pandemic_data)
    print()
    print("Masculino:")
    print(masc_pandemic_labels)
    print(masc_pandemic_data)
    print()
    print("Total:")
    print(pandemic_labels)
    print(pandemic_data)

main()