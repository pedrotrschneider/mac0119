'''
/******************************************************************************/
/*                                 Classe 02                                  */
/*                                                                            */
/* @brief Código feito para tratar dados e plotar gráficos relevantes para    */
/*        a entrega do exercício de MAE0119 Classe 02.                        */
/*                                                                            */
/* @authors Pedro Tonini Rosenberg Schneider,                                 */
/*          Guilherme Vinicius Ferreira de Assis.                             */
/*                                                                            */
/* @dependencies Esse programa depende apenas do módulo matplotlib de python. */
/*               Para mais informações sobre como instalá-lo, veja:           */
/*               <https://matplotlib.org/stable/users/installing.html>        */
/*                                                                            */
/* @usage                                                                     */
/* $ python3 main.py                                                          */
/*                                                                            */
/* @license GNU GPLv3                                                         */
/*                                                                            */
/*    This program is free software: you can redistribute it and/or modify    */
/*    it under the terms of the GNU General Public License as published by    */
/*    the Free Software Foundation, either version 3 of the License, or       */
/*    (at your option) any later version.                                     */
/*                                                                            */
/*    This program is distributed in the hope that it will be useful,         */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*    GNU General Public License for more details.                            */
/*                                                                            */
/*    You should have received a copy of the GNU General Public License       */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.  */
/*                                                                            */
/******************************************************************************/
'''

import csv
import matplotlib.pyplot as plot


def get_col(data:list, col:int, integer=False) -> list:
    '''
    /********************************************************************************
     * 
     * @brief Retira uma coluna determinada por (row) da matriz (data), pulando 
     *        a primeira entrada da coluna para levar em conta o cabeçalho dos dados.
     *
     * @param data Matriz de dados a ser retirada uma coluna.
     * @param col Determina qual a coluna a ser retirada da matriz. Indexado
     *            em 0, da esquerda para a direita.
     * @param integer Determina se os dados devem ser convertidos
     *                para inteiros (True) ou não (False);
     * 
     * @return Retorna uma lista com com a coluna de dados.
     *
     *******************************************************************************/
    '''
    out = []
    inp = 0
    for item in data:
        if inp != 0:
            if not integer:
                out.append(item[col])
            else:
                if int(item[col]) >= 0:
                    out.append(int(item[col]))
        inp += 1
    return out


def calc_freq(data: list) -> (list, list):
    '''
    /******************************************************************************
     * 
     * @brief Calcula a frequência absoluta dos dados de uma lista com valores
     *        discretos.
     * 
     * @param data Matriz de dados a ser retirada uma coluna.
     * 
     * @return Retorna um touple de duas listas, a primeira sendo as diferentes
     *         categorias que foram econtradas e a segunda a frequência absoluta
     *         de cada uma das categorias. As listas são alinhadas.
     *
     *****************************************************************************/
    '''
    dict = {}
    for item in data:
        if not item in dict:
            dict[item] = 0
        dict[item] = dict[item] + 1

    keys = list(dict)
    vals = []
    for key in dict:
        vals.append(dict[key])
    return (keys, vals)


def calc_freq_ranges(data: list, ranges: list) -> (list, list):
    '''
    /******************************************************************************************
     * 
     * Calcula a frequência absoluta dos dados de uma lista com valores contínuos, dada
     * uma lista de intervalos de valores
     * 
     * @param data Matriz de dados a ser retirada uma coluna.
     * @param ranges Lista de intervalos de valores a serem considerados no cálculo da
     *               frequência abosluta.
     * 
     * @return Retorna um touple de duas listas, a primeira sendo as diferentes
     *         categorias que foram econtradas e a segunda a frequência absoluta
     *         de cada uma das categorias. As listas são alinhadas.
     *
     *****************************************************************************************/
    '''

    labels = []
    vals = []
    for l in range(len(ranges) - 1):
        labels.append(f"{ranges[l]} a {ranges[l+1]}")
        vals.append(0)
    
    for item in data:
        for l in range(len(ranges) - 1):
            if ranges[l] <= item < ranges[l+1]: vals[l] += 1
    
    return (labels, vals)


def main():
    # Lendo arquivo .csv
    # Obs: os valores em branco nos dados foram substituídos por -1 para serem tratados mais facilmente.
    data = []
    with open("../data/respostas.csv", newline="") as file:
        data = list(csv.reader(file))

    ''' ************************************************
    Calculando e plotando as informações sobre o local.
    ************************************************ '''
    # Trataqndo os dados:
    place_col = get_col(data, 6)
    freq_labels, freq_data = calc_freq(place_col)
    print("Local: ", freq_labels, sep="")
    print("       ", freq_data, sep="")

    # Plotando os gráficos:
    plot.pie(freq_data, # Dados para plotar o gráfico.
             labels=freq_labels, # Labels para os dados. Deve estar alinhada com a lista de dados.
             shadow=True, # Desenha uma sombra em baixo do gráfico.
             autopct="%1.1f%%" # Faz com que as porcentagens sejam desenhadas, com precisão de uma casa decimal.
    )
    plot.suptitle("Distribuição de frequência de local") # Título que aparece na parte superior do gráfico
    plot.show() # Mostra uma janela com o gráfico. Tem que ter uma chamada pra essa função toda vez que plotar um gráfico.

    ''' *****************************************************
    Calculando e plotando as informações sobre os moradores.
    ****************************************************** '''
    # Tratndo os dados:
    housemates_col = get_col(data, 5)
    house_labels, house_data = calc_freq(housemates_col)
    for i in range(len(house_labels)): # Adicionando a palavra "moraderes" ao final de cada label.
        suffix = " morador"
        if(house_labels[i] != "1"): suffix += "es"
        house_labels[i] = house_labels[i] + suffix
    print("Moradores: ", house_labels, sep="")
    print("           ", house_data, sep="")

    # Plotando os gráficos
    plot.pie(house_data, # Dados para plotar o gráfico.
             labels=house_labels, # Labels para os dados. Deve estar alinhada com a lista de dados.
             shadow=True, # Desenha uma sombra em baixo do gráfico.
             autopct="%1.1f%%" # Faz com que as porcentagens sejam desenhadas, com precisão de uma casa decimal.
    )
    plot.suptitle("Distribuição de frequência de moradores") # Título que aparece na parte superior do gráfico
    plot.show() # Mostra uma janela com o gráfico. Tem que ter uma chamada pra essa função toda vez que plotar um gráfico.

    ''' **********************************************
    Calculando e plotando as informações sobre altura.
    ********************************************** '''
    # Tratando os dados
    height_col = get_col(data, 2, True)
    height_labels, height_data = calc_freq_ranges(height_col, range(152, 206, 6))
    print("Altura: ", height_labels, sep="")
    print("        ", height_data, sep="")

    # Plotado o gráfico:
    plot.hist(height_col, # Dados para plotar o histograma.
              range(152, 206, 6), # Intervalos de valores para o gráfico.
              density=True, # Mostra os dados na forma de densidade de frequência.
              edgecolor='black', # Mostra um outline preto nas caixas.
              linewidth=1.2 # Determina a espessura do outline nas caixas.
    )
    plot.xlim([150, 202]) # Determina os limites superior e inferior do eixo x.
    plot.xticks(range(152, 206, 6)) # Determina quais serão os valores marcados no eixo x.
    plot.suptitle("Histograma de alturas") # Título que aparece na parte superior do gráfico.
    plot.xlabel("Altura (cm)") # Label do eixo x.
    plot.ylabel("Densidade de frequência") # Label do eixo y.
    plot.show() # Mostra uma janela com o gráfico. Tem que ter uma chamada pra essa função toda vez que plotar um gráfico.

    ''' *********************************************
    Calculado e plotando as informações sobre peso.
    ********************************************* '''
    # Tratando os dados:
    weight_col = get_col(data, 3, True)
    weight_labels, weight_data = calc_freq_ranges(weight_col, range(40, 132, 15))
    print("Peso: ", weight_labels, sep="")
    print("      ", weight_data, sep="")

    # Plotando o gráfico:
    plot.hist(weight_col, # Dados para plotar o histograma.
              range(40, 160, 15), # Intervalos de valores para o gráfico.
              density=True, # Mostra os dados na forma de densidade de frequência.
              edgecolor='black', # Mostra um outline preto nas caixas.
              linewidth=1.2 # Determina a espessura do outline nas caixas.
    )
    plot.xlim([38, 132])  # Determina os limites superior e inferior do eixo x.
    plot.xticks(range(40, 132, 15)) # Determina quais serão os valores marcados no eixo x.
    plot.suptitle("Histograma de pesos") # Título que aparece na parte superior do gráfico.
    plot.xlabel("Peso (Kg)")  # Label do eixo x.
    plot.ylabel("Densidade de frequência") # Label do eixo y.
    plot.show() # Mostra uma janela com o gráfico. Tem que ter uma chamada pra essa função toda vez que plotar um gráfico.

    ''' *****************************************************************
    Calculando e plotando as informações sobre horas de estudo semanais.
    ***************************************************************** '''
    # Tratando os dados:
    studyhrs_col = get_col(data, 7, True)

    # Plotando o gráfico:
    plot.boxplot(studyhrs_col, # Lista de dados.
                 manage_ticks=True, # Atualiza os ticks no eixo de dados dependendo dos valores do boxplot.
                 showfliers=True, # Mostra os outliers no gráfico.
                 sym="x", # Define o símbolo que será usado para mostrar os outliers.
                 patch_artist=True, # Pinta a caixa do boxplot (é possível configurar a cor).
    )
    plot.yticks(range(0, 50, 5)) # Determina quais serão os valores marcados no eixo y.
    plot.xticks([]) # Determina quais serão os valores marcados no eixo x.
    plot.suptitle("Boxplot das horas de estudo") # Título que aparece na parte superior do gpráfico.
    plot.grid() # Mostra a grid no gráfico.
    plot.ylabel("Horas de estudo")  # Label do eixo y.
    plot.show() # Mostra uma janela com o gráfico. Tem que ter uma chamada pra essa função toda vez que plotar um gráfico.
    
    ''' *****************************************************************
    Calculando e plotando as informações sobre altura dividida por sexo.
    ***************************************************************** '''
    # Tratando os dados:
    sex_col = get_col(data, 1)
    fem_height = []
    masc_height = []
    for i in range(len(sex_col)): # Separando as aulturas de cada sexo em listas diferentes.
        item = sex_col[i]
        if item == "masculino":
            masc_height.append(height_col[i])
        else:
            fem_height.append(height_col[i])

    # Plotando o gáfico:
    plot.boxplot([fem_height, masc_height], # Lista de dados. Cada lista será representada como um boxplot diferente.
                 manage_ticks=True, # Atualiza os ticks no eixo de dados dependendo dos valores do boxplot.
                 showfliers=True, # Mostra os outliers no gráfico.
                 sym="x", # Define o símbolo que será usado para mostrar os outliers.
                 patch_artist=True, # Pinta a caixa do boxplot (é possível configurar a cor).
                 labels=["feminino", "masculino"] # Lista de labels para o gráfico. Cada string representa a label de um boxplot diferente.
    )
    plot.suptitle("Boxplot das alturas por sexo") # Título que aparece na parte superior do gráfico.
    plot.grid() # Mostra a grid no gráfico.
    plot.xlabel("Sexo") # Label do eixo x.
    plot.ylabel("Altura (cm)") # Label do eixo y.
    plot.show() # Mostra uma janela com o gráfico. Tem que ter uma chamada pra essa função toda vez que plotar um gráfico.


main()